# APPUNTI GIT - Version Control

Git è un software che serve per controllare quello che succede all’interno del codice sorgente della nostra applicazione.

AREA DI STAGING: È un'area temporanea dove puoi accumulare le modifiche prima di confermarle con un commit.

WORKING DIRECTORY: directory di lavoro che contiente i file attuali.

HEAD: punta all'ultimo commit fatto.

---

### Configurazione globale GIT
*git config --global user.name "Nome"*
--> per impostare il nome utente per i commit <br>

*git config --global user.email "@email.com*" --> per impostare l'indirizzo mail associao ai commit

### COMANDI PRINCIPALI

 *git init* + nome della directory 
 - Crea la repository
 - L'esecuzione git init crea una gitsottodirectory(.git) nella directory appena creata e contiene tutti i metadati Git necessari per il nuovo repository.

 *git status*
 - tiene traccia della posizione in cui si trova  il file
 - ci avvisa se c'è qualcosa da committare
 - se sono presenti dei file non ancora aggiunti nell'area di staging

 *git commit*
 - Effettua il commit. 
 - Avvia un editor di testo nel quale bisognerà inserire un messaggio di commit

 - *git commit -m "titolo commit"* <br>
  si può specificare il messaggio del commit direttamente nella riga di comando, senza aprire l'editor.
- *git commit --amend* <br>
è utilizzato per modificare il messaggio dell'ultimo commit eseguito senza creare un nuovo commit separato.

*git add nomeFile*
- aggiunge i fle all'area di staging
- *git add .* <br>
aggiunge tutti i file della directory in cui si trova all'area di staging

*git restore --staged*
- rimuove i file dall'area di staging, ma lascia invariate le modifiche
- *git restore nomeFile* ripristina il file al suo stato precedente all'ultimo commit 

*git rm nomeFile*
- rimuove il file dalla working directory e lo aggiunge all'area di staging

- *git rm --cached nomeFile* <br>
rimuove il file dall'area di staging e dal repository locale, il file rimane intatto nella working directory.

*git mv*
- serve per spostare o rinominare i file

*.gitignore*
- usato quando si hanno dei file che non vogliamo committare(es.swp file o node modules). All'intrno del file .gitignore possiamo elencare i file che vogliamo ignorare nel repository Git.
(Se un file è già stato tracciato da Git prima di essere aggiunto a .gitignore, bisogna rimuoverlo dall'area di staging e dal repository locale utilizzando git rm --cached <file> per smettere di tracciare le modifiche in futuro senza eliminarlo dalla working directory.)

*git log*
- usato per visualizzare la cronologia(dal più recente al meno) dei commit
(chi ha fatto cosa dove e quando).
Mostra hash del commit, autore,data e messaggio di commit.
- *git log --oneline* <br>
Questo comando mostra la cronologia dei commit in una forma compatta. Ogni commit è visualizzato su una singola riga, rendendo più facile ottenere una panoramica rapida della cronologia del progetto. L'output include l'hash del commit e il messaggio del commit.
- *git log --all* <br>
come git log, ma visualizza la cronologia dei commit di tutti i branch.

*git show headCommit*
- mostra le informazioni dettagliate relative al commit specificato. Se si usa solo *git show HEAD* mostra il commit più recente del branch in cui ci si trova.

*git push nomeRepositoryRemoto NomeBranch*
- invia i commit local al repository remoto <br>
*git push origin main* 
- comndo importante per condividere il lavoro con colleghi e aggiornare repository remoto con le modifiche locali
- quando si è sul branch main e si vogliono mandare i commit al repository remoto.
- *git push --force* o *git push --f* <br>
forza il push di commit locali su un repository remoto, sovrascrivendo la storia del repository remoto con quella locale.
DA USARE CON CAUTELA! sovrascrive la storia del repository senza chiedere conferma, è possibile perdere i dati se altri utenti hanno già basato il loro lavoro sui commit esistenti nel repository remoto.


*git diff*
- mostra le differenze tra i commit, tra le diverse versione di file.
- mostra le differenze tra i file nell'area di staging e quelli nella working directory (file modificati ma non ancora aggiunti all'area di staging) <br>
- *git diff --stat*
mostra un riepilogo delle modifiche in una forma più compatta, fornendo una panoramica dei file che sono cambiati e di quanto.

*git checkout*
- utilizzato per spostarsi tra i branch: cambiare branch (*git checkout nomeBranch*)
- navigare tra i commit (*git checkout hashCommit*)
- ripristinare le modifiche fatte a dei file <br>
(*git checkout -- nomefile*) <br>
- *git checkout -b*
crea un nuovo branch e ti porta ll'interno di esso <br>

*git rebase* {operazione avanzata di GIT, usare con cautela!} <br>
- per riallineare un branch con un altro.
- per appendere, modificare, riunire più cose in un unico commit

*git rebase nomeBranchDestinazione*
- sposta una serie di commit da un branch su un altro

*git pull*
- Il suo scopo principale è quello di applicare le modifiche dal repository remoto al repository locale. 
- Recupera i commit e le modifiche dal repository remoto e applica i commit recuperati nel branch locale corrente. Se ci sono modifiche conflittuali, crea un commit di merge. <br>

*git pull origin main*
recupera i commit dal branch main di origin e li integra nel branch locale main.

*git clone*
- per creare una copia locale (clone) di un repository Git esistente dal repository remoto.
- copiare url da gitLab e clona il repository in locale per lavorarci sopra direttamente.(*git clone https://gitlab.com/utente/nome-repository.git*
)
- permette di avere una copia locale del repository

*git fetch*
- per aggiornare il repository locale con le ultime modifiche dal repository remoto senza fondere automaticamente i cambiamenti nel ramo attuale.

#### BRANCH
Quando si desidera lavorare su nuove modifiche senza influenzare il ramo principale del progetto, si può creare un nuovo ramo. Questo nuovo ramo consente di sperimentare, testare e apportare modifiche in modo isolato. È utile perché mantiene il ramo principale, di solito chiamato main, stabile e funzionante per tutti gli altri collaboratori.

Una volta completate le modifiche nel nuovo ramo, è possibile unire (merge) le modifiche nel ramo principale (main). Questo processo permette di integrare le modifiche testate e funzionanti nel flusso principale di sviluppo del progetto.
future e tip

#### MERGE REQUEST
Serve per unire i branch e le loro modifiche, ed è una richiesta vera e  propria per poter applicare delle modifche. Per far si che le modifiche diventino ufficiali vanno confermate dal proprietario del repository (master).

*git merge nomeBranch*
- Serve per integrare le modifiche da un altro branch nel branch corrente.

*git commit sign*

*git reset nomeCommit*

- *git reset --hard nomeCommit*<br>
elimina completamente tutti i cambiamenti non commessi nel working directory, riportandolo allo stato del commit specificato. Questo comando è drastico e può portare alla perdita permanente di modifiche non salvate.

*git revert nomeCommit*
- crea un nuovo commit che annulla le modifiche apportate nel commit specificato 

*git cherrypick hashCommit*
- è la stessa cosa del revert ma con segno opposto (revert in negativo e cherrypick in positivo)
- utilizzato per applicare i cambiamenti introdotti da un commit specifico (o una serie di commit) in un altro branch
- utile quando si vuole fare una modifica specifica da un branch a un altro senza dover fare un merge completo

*git blame nomeFile*
- mostra ogni linea del file nomefile con l'hash del commit, l'autore e la data della modifica.


*git add -p*
 - Git analizza le modifiche non ancora commesse nel working directory e presenta un'interfaccia interattiva per selezionare le porzioni delle modifiche da aggiungere.


*git tag*
- elenca tutti i tag presenti nella repository
- viene utilizzato per creare etichette (tag) sui commit. I tag sono utili per marcare punti specifici nella storia di un repository, come le versioni di rilascio --> (*git tag nomeTag*)


*git stash*
-  per salvare temporaneamente le modifiche della mia repository locale. Questo è utile quando si desidera passare a un altro branch o lavorare su qualcosa di urgente senza perdere le modifiche attuali.
- *git stash pop*<br>
Recupera e applica le modifiche dall'ultimo stash, rimuovendolo dalla lista degli stash.



